terraform-vsphere-example
========================

This is an example of using terraform with vshere.

Notice: This is fairly untested since I don't currently have a good vsphere+esxi setup

Usage
--------------

1. `git clone` this repo

2. add a file called `terraform.tfvars` with the following content
    ```bash
    vsphere_user = "adminuser@server"
    vsphere_password = "somepassword"
    ```
    Keep this away from git or vault it

3. Update `variables.tf`with default values matching your setup.

3. `terraform plan`

4. `terraform apply`


and use `terraform destroy` to clean up.
