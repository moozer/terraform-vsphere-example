# vsphere vars
variable "vsphere_server" {
    default = "my_vsphere_ip_or_server_name"
}
variable "vsphere_datacenter" {
    default = "my_datacenter"
}
variable "vsphere_datastore" {
    default ="my_datastore"
}
variable "vsphere_resource_pool" {
    default = "terraform"
}

# user + pass: no default
variable "vsphere_user" {}
variable "vsphere_password" {}

# VM vars
variable "vsphere_virtual_machine_template" {
    default = "my_base_template"
}
variable "vsphere_network" {
    default = "my_network"
}
