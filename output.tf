output "ip" {
    value = {
        for k, v in vsphere_virtual_machine.servers : k => v.default_ip_address
    }
}
output "vm-moref" {
    value = {
        for k, v in vsphere_virtual_machine.servers : k => v.moid
    }
}
